package com.thirdyearproject;

import java.io.FileInputStream;
import java.util.*;

import com.google.common.io.ByteStreams;

import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.Classifier;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.Domain;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.DomainType;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.ImageFileCreateBatch;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.ImageFileCreateEntry;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.Iteration;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.Project;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.Region;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.TrainProjectOptionalParameter;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.CustomVisionTrainingClient;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.Trainings;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.CustomVisionTrainingManager;
import com.microsoft.azure.cognitiveservices.vision.customvision.prediction.models.ImagePrediction;
import com.microsoft.azure.cognitiveservices.vision.customvision.prediction.models.Prediction;
import com.microsoft.azure.cognitiveservices.vision.customvision.prediction.CustomVisionPredictionClient;
import com.microsoft.azure.cognitiveservices.vision.customvision.prediction.CustomVisionPredictionManager;
import com.microsoft.azure.cognitiveservices.vision.customvision.training.models.Tag;
import rx.Observable;
import rx.functions.Func1;

public class CustomVision {

    final String trainingApiKey = "9a8aa6d412f445a3807f981d84cf16b3";
    final String predictionApiKey = "fe405dc754804b4695ce5d2b1a626ed2";
    final String endpoint = "https://southcentralus.api.cognitive.microsoft.com/";
    final String predictionResourceId = "/subscriptions/1577b4ee-575f-4620-8a4f-82e0c8604854/resourceGroups/ACAP-Project/providers/Microsoft.CognitiveServices/accounts/ACAP-Prediction";

    Tag dogTag;
    Tag catTag;
    Iteration iteration;
    String publishedModelName;
    Project project;
    UUID projectID;
    CustomVisionTrainingClient trainClient;
    CustomVisionPredictionClient predictor;

    Boolean modelOn = true;

    public CustomVision() throws InterruptedException {
        trainClient = CustomVisionTrainingManager
                .authenticate("https://{endpoint}/customvision/v3.0/training/", trainingApiKey)
                .withEndpoint(endpoint);
        predictor = CustomVisionPredictionManager
                .authenticate("https://{endpoint}/customvision/v3.0/prediction/", predictionApiKey)
                .withEndpoint(endpoint);

        projectID = UUID.fromString("6e41f07a-4510-4cef-8f77-c67e53d8cba0");
        project = trainClient.trainings().getProject(projectID);

        addTags();
        if (modelOn) {
            uploadImages();
            trainProject();
            publishIteration();
        }
        //testProject(predictor, project);
    }

    public void addTags() {

        Trainings trainer = trainClient.trainings();
        try {
            // create dog tag
            dogTag = trainer.createTag().withProjectId(project.id()).withName("dog").execute();
            // create cat tag
            catTag = trainer.createTag().withProjectId(project.id()).withName("cat").execute();
        }
        catch (Exception e){
            modelOn = false;
        };
    }

    public void uploadImages() {

        Trainings trainer = trainClient.trainings();
        System.out.println("Adding images...");
        for (int i = 1; i <= 44; i++) {
            String fileName = "dog_" + i + ".jpg";
            byte[] contents = GetImage("./Dogs", fileName);
            AddImageToProject(trainer, project, fileName, contents, dogTag.id(), null);
        }

        for (int i = 1; i <= 44; i++) {
            String fileName = "cat_" + i + ".jpg";
            byte[] contents = GetImage("./Cats", fileName);
            AddImageToProject(trainer, project, fileName, contents, catTag.id(), null);
        }
    }

    private void AddImageToProject(Trainings trainer, Project project, String fileName, byte[] contents,UUID tag, double[] regionValues) {

        System.out.println("Adding image: " + fileName);
        ImageFileCreateEntry file = new ImageFileCreateEntry().withName(fileName).withContents(contents);

        ImageFileCreateBatch batch = new ImageFileCreateBatch().withImages(Collections.singletonList(file));

        // If Optional region is specified, tack it on and place the tag there,
        // otherwise
        // add it to the batch.
        if (regionValues != null) {
            Region region = new Region().withTagId(tag).withLeft(regionValues[0]).withTop(regionValues[1])
            .withWidth(regionValues[2]).withHeight(regionValues[3]);
            file = file.withRegions(Collections.singletonList(region));
        }
        else
            batch = batch.withTagIds(Collections.singletonList(tag));

        trainer.createImagesFromFiles(project.id(), batch);
    }

    private byte[] GetImage(String folder, String fileName) {
        try {
            return ByteStreams.toByteArray(new FileInputStream(folder + "/" + fileName));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public void trainProject() throws InterruptedException {
        System.out.println("Training...");
        Trainings trainer = trainClient.trainings();

        iteration = trainer.trainProject(project.id(), new TrainProjectOptionalParameter());

        while (iteration.status().equals("Training")) {
            System.out.println("Training Status: " + iteration.status());
            Thread.sleep(1000);
            iteration = trainer.getIteration(project.id(), iteration.id());
        }
        System.out.println("Training Status: " + iteration.status());
    }

    public void publishIteration() {
        Trainings trainer = trainClient.trainings();
        // The iteration is now trained. Publish it to the prediction endpoint.
        publishedModelName = "ACAP-model";
        trainer.publishIteration(project.id(), iteration.id(), publishedModelName, predictionResourceId);
    }

    // load test image
    public Boolean testProject(String imageName) {

        byte[] testImage = GetImage("./MessagesMedia", imageName + ".jpg");

        // predict

        ImagePrediction results = predictor.predictions().classifyImage().withProjectId(project.id())
                .withPublishedName(publishedModelName).withImageData(testImage).execute();

        System.out.println("Predicted" + imageName);

        Prediction dogPrediction = null;
        Prediction catPrediction = null;

        for (Prediction prediction : results.predictions()) {
            if (prediction.tagName().equals("dog"))
                dogPrediction = prediction;
            else
                catPrediction = prediction;
        }

        if ((catPrediction.probability()*100.0f) > 50.0f)
            return true;
        else
            return false;
    }
}
