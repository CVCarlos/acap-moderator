package com.thirdyearproject;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.*;
import org.asynchttpclient.*;
import org.bytedeco.javacv.FrameGrabber;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Hello world!
 *
 */
public class App
{
    private static StringToWordVector wekaFilter;
    private static J48 wekaTree;

    public static volatile ArrayList<Thread> classifyList= new ArrayList<>();
    public static volatile ArrayList<Thread> mediaClassifyList= new ArrayList<>();
    public static volatile Map<Thread, ScheduledFuture>  mediaClassifyListScheduled= new HashMap<Thread, ScheduledFuture>();

    public static volatile  DatabaseReference ref;

    public static void main( String[] args ) throws Exception {

        System.out.println( "Hello World!" );

        CustomVision customVision = new CustomVision();

        Weka wekaInstance = new Weka();

        wekaInstance.buildClassifier();

        wekaFilter = wekaInstance.getFilter();
        wekaTree = wekaInstance.getTree();

        FileInputStream serviceAccount = new FileInputStream("./serviceAccountKey.json");

        Map<String, Object> auth = new HashMap<String, Object>();
        auth.put("uid", "my-service-worker");

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://third-year-project-5bf87.firebaseio.com")
                .setDatabaseAuthVariableOverride(auth)
                .build();

        FirebaseApp.initializeApp(options);

        // As an admin, the app has access to read and write all data, regardless of Security Rules
        ref = FirebaseDatabase.getInstance().getReference();

        classifyCheckerThread classifyChecker = new classifyCheckerThread();
        classifyChecker.start();

        classifyMediaCheckerThread classifyMediaChecker = new classifyMediaCheckerThread();
        classifyMediaChecker.start();

        ArrayList<String> mediaReceived = new ArrayList();

        HashMap<String, File> chats = new HashMap<>();
        HashMap<String, Integer> amountOfMessageBySender = new HashMap<>();
        HashMap<String, Thread> messageCheckerThread = new HashMap<>();

        readMessagesThread readMessages = new readMessagesThread(ref);
        readMessages.start();

        ScheduledThreadPoolExecutor scheduledExecutorService = new ScheduledThreadPoolExecutor(1);
        scheduledExecutorService.setRemoveOnCancelPolicy(true);

        int messageListSize = 0;
        ArrayList<Message> messages;
        while(true) {
            if (readMessages.getUpdated()) {
                messages = readMessages.getMessageList();
                for (int i = messageListSize; i < messages.size(); i++)
                {
                    if (messages.get(i).getMessage() != null) {
                        String sender = messages.get(i).getSender();
                        if (!chats.containsKey(messages.get(i).getSender())) {
                            File file = new File("./MessagesText/" + sender + ".arff");
                            if (file.exists())
                                file.delete();
                            file.createNewFile();
                            chats.put(sender, file);
                            FileWriter fileWriter = new FileWriter(file, true);
                            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                            bufferedWriter.write("@relation 'Chats Moderation'\n\n");
                            bufferedWriter.write("@attribute Text String\n");
                            bufferedWriter.write("@attribute class-att {0,1}\n\n");
                            bufferedWriter.write("@data\n");
                            bufferedWriter.write("'" + messages.get(i).getMessage() + "',0\n");
                            bufferedWriter.close();
                            amountOfMessageBySender.put(sender, 1);
                        } else {
                            Integer amountOfMessages = amountOfMessageBySender.get(sender);
                            amountOfMessages++;
                            amountOfMessageBySender.put(sender, amountOfMessages);
                            File chat = chats.get(messages.get(i).getSender());
                            FileWriter fileWriter = new FileWriter(chat, true);
                            fileWriter.write("'" + messages.get(i).getMessage() + "',0\n");
                            fileWriter.close();
                            if (amountOfMessages > 5) {
                                if (messageCheckerThread.containsKey(sender))
                                    ((classifyChatsThread) messageCheckerThread.get(sender)).wait = false;
                                else {
                                    classifyChatsThread classifyThread = new classifyChatsThread(wekaFilter, wekaTree, "./MessagesText/" + sender + ".arff", sender);
                                    classifyThread.start();
                                    classifyList.add(classifyThread);
                                    messageCheckerThread.put(sender, classifyThread);
                                }
                            }
                        }
                    }
                    else if (messages.get(i).getMedia() != null){
                        //Classify images
                        Message message = messages.get(i);
                        URL url = new URL((message.getMedia()));
                        BufferedImage img = ImageIO.read(url);
                        if (!mediaReceived.contains(message.getID())) {
                            mediaReceived.add(message.getID());
                            File file = new File("./MessagesMedia/" + message.getID() + ".jpg");
                            ImageIO.write(img, "jpg", file);
                            classifyMediaThread mediaThread = new classifyMediaThread(customVision, message.getID(), message.getSender());
                            ScheduledFuture<?> scheduledFuture = scheduledExecutorService.scheduleWithFixedDelay(mediaThread, 0, 2, TimeUnit.SECONDS);
                            mediaClassifyList.add(mediaThread);
                            mediaClassifyListScheduled.put(mediaThread,scheduledFuture);

                            //mediaThread.start();
                        }
                    }
                    else {
                        //Classify video
                        Message message = messages.get(i);
                        classifyVideoThread videoThread = new classifyVideoThread(customVision, message.getVideo(),message.getID(), message.getSender(),scheduledExecutorService);
                        videoThread.run();
                        //                        if (!mediaReceived.contains(message.getID())) {
//                            mediaReceived.add(message.getID());
//                            File file = new File("./MessagesMedia/" + message.getID() + ".jpg");
//                            ImageIO.write(img, "jpg", file);
//                            classifyMediaThread mediaThread = new classifyMediaThread(customVision, message.getID(), message.getSender());
//                            ScheduledFuture<?> scheduledFuture = scheduledExecutorService.scheduleWithFixedDelay(mediaThread, 0, 2, TimeUnit.SECONDS);
//                            mediaClassifyList.add(mediaThread);
//                            mediaClassifyListScheduled.put(mediaThread,scheduledFuture);
//
//                            //mediaThread.start();
//                        }
                    }
                }
                messageListSize = messages.size();
            }
        }
    }

    public static class readMessagesThread extends Thread {

        DatabaseReference reference;
        volatile ArrayList<Message> messagesList = new ArrayList<>();
        volatile Boolean updated;

        public readMessagesThread(DatabaseReference ref) {
            super();
            updated = false;
            reference = ref.child("Chats");
        }

        @Override
        public void run() {
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    messagesList.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren())
                    {
                        Message message = snapshot.getValue(Message.class);
                        message.setID(snapshot.getKey());
                        messagesList.add(message);
                    }
                    updated = true;
                }
                @Override
                public void onCancelled(DatabaseError error) {
                }
            });
        }

        public ArrayList<Message> getMessageList(){
            return messagesList;
        }

        public Boolean getUpdated(){
            if (updated){
                updated = false;
                return  true;
            }
            return updated;
        }
    }

    public static class classifyChatsThread extends Thread {

        StringToWordVector filterToUser;
        J48 treeToUse;
        String chatFile;
        String sender;
        Boolean result;
        Integer lastCounted = 0;
        Integer counterNegative = 0;
        Integer counterPositive = 0;
        volatile Boolean completed = false;
        volatile Boolean run = true;
        volatile Boolean wait = true;

        public classifyChatsThread(StringToWordVector filter, J48 tree, String chat, String id) {
            super();
            filterToUser = filter;
            treeToUse = tree;
            chatFile = chat;
            sender = id;
        }

        @Override
        public void run() {
            try {
                while(run) {
                    ConverterUtils.DataSource sourceTest = new ConverterUtils.DataSource(chatFile);
                    Instances datasetTest = sourceTest.getDataSet();

                    datasetTest.setClassIndex(datasetTest.numAttributes() - 1);

                    datasetTest = Filter.useFilter(datasetTest, filterToUser);

                    int i;
                    for (i = lastCounted; i < datasetTest.numInstances(); i++) {
                        int prediction = (int) treeToUse.classifyInstance(datasetTest.instance(i));
                        String label = datasetTest.classAttribute().value((prediction));
                        System.out.println(label);
                        if (label.trim().equals("0"))
                            counterNegative++;
                        else
                            counterPositive++;
                    }
                    lastCounted = i;
                    if (((counterPositive * 100) / (counterNegative + counterPositive)) > 30) {
                        result = true;
                    } else {
                        result = false;
                    }
                    completed = true;
                    wait = true;
                    while (wait) {
                    }
                }

            }
            catch (Exception e)
            {}
        }

        public Boolean getCompleted() {
            return completed;
        }

        public Boolean getResult() {
            return result;
        }
    }

    public static class classifyCheckerThread extends Thread {

        public classifyCheckerThread() {
            super();
        }

        @Override
        public void run() {
            while (true) {
                //System.out.println(App.classifyList.size());
                for (int i = 0; i < App.classifyList.size(); i++) {
                    classifyChatsThread thread = (classifyChatsThread) App.classifyList.get(i);
                    if (thread.getCompleted()) {
                        if (thread.getResult()) {
                            System.out.println("found");
                            ApiFuture future = App.ref.child("banned_uids").child(thread.sender).setValueAsync(true);
                            while (!future.isDone())
                            {}
                            App.classifyList.remove(thread);
                            thread.run = false;
                            thread.wait = false;
                            // Important this is blocking!!!
                        }
                    }
                }
            }

        }
    }

    public static class classifyMediaThread extends Thread {

        CustomVision customVision;
        String mediaFile;
        String sender;
        Boolean result;
        volatile Boolean completed = false;
        volatile Boolean run = true;

        public classifyMediaThread(CustomVision customVisionToUse, String media, String id) {
            customVision = customVisionToUse;
            mediaFile = media;
            sender = id;
        }

        @Override
        public void run() {
            result = customVision.testProject(mediaFile);
            completed = true;
            while (run){}
        }

        public Boolean getCompleted() {
            return completed;
        }

        public Boolean getResult() {
            return result;
        }
    }

    public static class classifyMediaCheckerThread extends Thread {

        public classifyMediaCheckerThread() {
        }

        @Override
        public void run() {
            while (true) {
                //System.out.println(App.classifyList.size());
                for (int i = 0; i < App.mediaClassifyList.size(); i++) {
                    classifyMediaThread thread = (classifyMediaThread) App.mediaClassifyList.get(i);
                    if (thread.getCompleted()) {
                        if (thread.getResult()) {
                            System.out.println("found");
                            ApiFuture future = App.ref.child("banned_uids").child(thread.sender).setValueAsync(true);
                            while (!future.isDone())
                            {}
                            // Important this is blocking!!!
                        }
                        thread.run = false;
                        App.mediaClassifyList.remove(thread);
                        App.mediaClassifyListScheduled.get(thread).cancel(false);
                        App.mediaClassifyListScheduled.remove(thread);
                    }
                }
            }

        }
    }

    public static class classifyVideoThread extends Thread {

        CustomVision customVision;
        ScheduledThreadPoolExecutor scheduler;

        String videoFile;
        String videoURL;
        String sender;
        Boolean result;
        volatile Boolean completed = false;
        volatile Boolean run = true;

        public classifyVideoThread(CustomVision customVisionToUse, String url, String video, String id, ScheduledThreadPoolExecutor schedulerToUse) {
            this.customVision = customVisionToUse;
            this.videoURL = url;
            this.videoFile = video;
            this.sender = id;
            this.scheduler = schedulerToUse;
        }


        @Override
        public void run() {
            try {
                completed = false;
                AsyncHttpClient client = Dsl.asyncHttpClient();
                final FileOutputStream stream = new FileOutputStream("./MessagesVideo/" + videoFile + ".mp4");
                ListenableFuture downloader = client.prepareGet(videoURL).execute(new AsyncCompletionHandler<FileOutputStream>() {

                    @Override
                    public State onBodyPartReceived(HttpResponseBodyPart bodyPart)
                            throws Exception {
                        stream.getChannel().write(bodyPart.getBodyByteBuffer());
                        return State.CONTINUE;
                    }

                    @Override
                    public FileOutputStream onCompleted(Response response) throws Exception {

                        return stream;
                    }
                });
                while(!downloader.isDone()){}
                System.out.println("here");
                VideoFraming framing = new VideoFraming(videoFile);
                System.out.println("here-again");
                int totalFrames = framing.getCounter();
                int i = 0;
                boolean result;
                do {
                    String path = "/" + videoFile + "Frames/" + videoFile + i;
                    classifyMediaThread mediaThread = new classifyMediaThread(customVision, path, sender);
                    ScheduledFuture schedule = scheduler.scheduleWithFixedDelay(mediaThread, 0, 2, TimeUnit.SECONDS);
                    while (!mediaThread.getCompleted()) {}
                    schedule.cancel(false);
                    scheduler.remove(mediaThread);
                    result = mediaThread.getResult();
                    mediaThread.run = false;
                    System.out.println(result);
                    if (result) {
                        System.out.println("found");
                        ApiFuture future = App.ref.child("banned_uids").child(sender).setValueAsync(true);
                        while (!future.isDone())
                        {}
                    }
                    i++;
                }
                while(i < totalFrames && result == false);
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            classifyVideoThread.this.run = false;
        }
    }

    public static class classifyVideoCheckerThread extends Thread {

        public classifyVideoCheckerThread() {
        }
    }
}
