package com.thirdyearproject;

import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.trees.J48;
import weka.core.Instances;

import weka.core.converters.ConverterUtils.DataSource;
import weka.core.stemmers.LovinsStemmer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;


public class Weka {

    public Weka() {
    }

    private StringToWordVector filter = new StringToWordVector();;
    private J48 tree = new J48();

    public void buildClassifier() throws Exception {

        DataSource source = new DataSource("./train.arff") ;
        Instances dataset = source.getDataSet();

        dataset.setClassIndex(dataset.numAttributes()-1);

        filter.setInputFormat(dataset);
        filter.setIDFTransform(true);
        LovinsStemmer stemmer = new LovinsStemmer();
        filter.setStemmer(stemmer);
        filter.setLowerCaseTokens(true);

        //Create the FilteredClassifier object
        FilteredClassifier fc = new FilteredClassifier();
        //specify filter
        fc.setFilter(filter);
        //specify base classifier
        fc.setClassifier(tree);
        //Build the meta-classifier
        fc.buildClassifier(dataset);

        System.out.println(tree.getCapabilities().toString());
        System.out.println(tree.graph());
    }

    public StringToWordVector getFilter() {
        return filter;
    }

    public J48 getTree() {
        return tree;
    }

    public Boolean classifyChat(String chatFile) throws Exception {
        //Classify
        DataSource sourceTest = new DataSource(chatFile);
        Instances datasetTest = sourceTest.getDataSet();

        datasetTest.setClassIndex(datasetTest.numAttributes()-1);

        datasetTest = Filter.useFilter(datasetTest,filter);

        Integer counterNegative = 0;
        Integer counterPositive = 0;

        for (int i = 0; i < datasetTest.numInstances(); i++) {
            int prediction = (int) tree.classifyInstance(datasetTest.instance(i));
            String label = datasetTest.classAttribute().value((prediction));
            if (label.trim().equals("0"))
                counterNegative++;
            else
                counterPositive++;
        }
        if (((counterPositive*100)/(counterNegative+counterPositive)) > 30)
            return true;
        else
            return false;
    }
}
