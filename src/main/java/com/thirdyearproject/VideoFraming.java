package com.thirdyearproject;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

import org.bytedeco.javacv.*;
import org.bytedeco.javacv.FrameGrabber.Exception;
import org.bytedeco.opencv.opencv_core.IplImage;

public class VideoFraming {

    int framesCounter = 0;

    public VideoFraming(String videoName) throws IOException, Exception {
        FFmpegFrameGrabber frameGrabber = new FFmpegFrameGrabber("./MessagesVideo/" + videoName + ".mp4");
        Java2DFrameConverter paintConverter = new Java2DFrameConverter();
        frameGrabber.start();
        Frame img;
        framesCounter = 0;
        int frameRate = (int) Math.ceil(frameGrabber.getFrameRate());
        new File("./MessagesMedia/" + videoName + "Frames").mkdirs();
        int counter = frameRate - 1;
        while ((img = frameGrabber.grab()) != null) {
            counter++;
            if (counter == frameRate){
                try {
                        BufferedImage bi = paintConverter.convert(img);
                        if (bi != null) {
                            ImageIO.write(bi, "jpg", new File("./MessagesMedia/" + videoName + "Frames/" + videoName + framesCounter + ".jpg"));
                            framesCounter++;
                        }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                counter = 1;
            }
        }
        frameGrabber.stop();
    }

    public int getCounter() {
        return framesCounter;
    }

}
