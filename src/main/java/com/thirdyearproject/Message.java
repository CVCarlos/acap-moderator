package com.thirdyearproject;

public class Message {

    private String sender;
    private String receiver;
    private String message;
    private String media;
    private String video;
    private String ID;

    public Message(String message, String receiver, String sender, String media, String ID) {
        this.message = message;
        this.receiver = receiver;
        this.sender = sender;
        this.media = media;
        this.ID = ID;
    }

    public Message() {

    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMedia() { return media; }

    public void setMedia(String media) { this.media = media; }

    public String getID() { return ID; }

    public void setID(String ID) { this.ID = ID; }

    public String getVideo() { return video; }

    public void setVideo(String video) { this.video = video; }

}
